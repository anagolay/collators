https://docs.substrate.io/reference/how-to-guides/parachains/connect-to-a-relay-chain/#obtain-the-webassembly-runtime-validation-function

Note that this setup will work without modifications only in the Anagolay infra servers. You should remove the `caddy` external network if you do not host it on 
Anagolay Servers.


Using the binary:
```sh

# build spec
./anagolay build-spec \
--disable-default-bootnode > ./specs/rococo/idiyanale-parachain-plain-4222.json


##### EDIT THE SPEC NOW BEFORE CONTINUE

# build raw-spec
./anagolay build-spec \
 --chain ./specs/rococo/idiyanale-parachain-plain-4222.json \
 --raw \
 --disable-default-bootnode > ./specs/rococo/idiyanale-parachain-raw-4222.json 

# parachain genesis wasm
./anagolay export-genesis-wasm --chain ./specs/rococo/idiyanale-parachain-raw-4222.json  > ./specs/rococo/para-wasm-4222

../anagolay export-genesis-wasm --chain ./output/local-4222.json  > ./output/genesis-wasm-4222.txt
../anagolay export-genesis-state --chain ./output/local-4222.json  > ./output/genesis-state-4222.txt


# genesis state
./anagolay export-genesis-state --chain ./specs/rococo/idiyanale-parachain-raw-4222.json > ./specs/rococo/para-genesis-4222

```
install https://github.com/open-web3-stack/parachain-launch


Using docker:

```sh
# sudo

## slots

contains the triggerOnboard 

## paras
  
  `forceQueueAction(para)` put parachain in the next tess queue

## parasSudoWrapper

`sudoScheduleParathreadUpgrade`



```

echo "Install polkadot executable to run relay chain"
wget -c https://github.com/paritytech/polkadot/releases/download/v0.9.32/polkadot -O ./artifacts/polkadot
chmod +x ./artifacts/polkadot

echo "Install zombienet executable to run a local network"
wget -c https://github.com/paritytech/zombienet/releases/latest/download/zombienet-linux-x64 -O ./artifacts/zombienet
chmod +x ./artifacts/zombienet

```

https://substrate.stackexchange.com/questions/1394/our-parachain-doesnt-produce-blocks-checklist