#!/usr/bin/env bash

########################################################################
## Example
## chmod +x create.sh
## ./create.sh --chain local --image registry.gitlab.com/anagolay/anagolay/idiyanale:3861d9b3 --parachain 4222
##############################################################################

##### this part taken from https://stackoverflow.com/a/20816534/2764898
SCRIPTNAME="${0##*/}"
OUTPUT_DIR="./parachain_out"
warn() {
  printf >&2 "$SCRIPTNAME: $*\n"
}

iscmd() {
  command -v "$@" >&-
}

checkdeps() {
  local -i not_found
  for cmd; do
    iscmd "$cmd" || {
      warn $"$cmd is not found"
      let not_found++
    }
  done
  ((not_found == 0)) || {
    warn $"Install dependencies listed above to use $SCRIPTNAME"
    exit 1
  }
}

# exit with the code and clean the history
function exit_clean_hist() {
  cat /dev/null >~/.bash_history
  exit 1
}

### deps check
checkdeps docker docker-compose jq wget

#######

# Convenience functions.
usage_error() {
  echo >&2 "$(basename $0):  $1"
  exit 2
}

function show_help() {
  USAGE="Usage:  ${CMD:=${0##*/}} [(-h|--help)] [--chain=TEXT] [--parachain=NUMBER] [--image=TEXT]"

  echo "Anagolay Collator Helper Script"
  echo ""
  printf "%s\n" "$USAGE"
  echo ""
  echo "  --chain             - chain spec name. Possible values: local, testnet, ..."
  echo "  --image             - OCI name. If provided in short form it will use docker hub."
  echo "  --parachain         - prachain ID. You obtain this through reserving it or sudo-ing the local relay chain"
  echo ""
  echo "  -h | --help         - Shows this help"
  echo ""
  exit 0
}

assert_argument() { test "$1" != "$EOL" || usage_error "$2 requires an argument"; }

# shamelessly taken and modified from https://stackoverflow.com/a/62616466/2764898
# One loop, nothing more.
if [ "$#" != 0 ]; then
  EOL=$(printf '\1\3\3\7')
  set -- "$@" "$EOL"
  while [ "$1" != "$EOL" ]; do
    opt="$1"
    shift
    case "$opt" in

    # Your options go here.
    --chain)
      assert_argument "$1" "$opt"
      chain="$1"
      shift
      ;;
    --image)
      assert_argument "$1" "$opt"
      image="$1"
      shift
      ;;
    --parachain)
      assert_argument "$1" "$opt"
      parachain="$1"
      shift
      ;;
    -h | --help)
      show_help
      exit 0
      ;;

    # Arguments processing. You may remove any unneeded line after the 1st.
    --*=*) set -- "${opt%%=*}" "${opt#*=}" "$@" ;; # convert '--name=arg' to '--name' 'arg'
    --) while [ "$1" != "$EOL" ]; do
      set -- "$@" "$1"
      shift
    done ;;                                             # process remaining arguments as positional
    -*) usage_error "unknown option: '$opt'" ;;         # catch misspelled options
    *) usage_error "this should NEVER happen ($opt)" ;; # sanity test for previous patterns
    esac
  done
  shift # $EOL
fi

if [[ -z "${chain}" ]]; then
  echo "chain name is not provided. Please add --chain flag with value [--chain local]"
  exit_clean_hist
fi
if [[ -z "${image}" ]]; then
  echo "image is not provided. Please add --image flag with value [--image anagolay/idiyanale:221121]"
  exit_clean_hist
fi
if [[ -z "${parachain}" ]]; then
  echo "parachain ID is not provided. Please add --parachain flag with value [--parachain 4222]"
  exit_clean_hist
fi

function create_spec() {
  if [[ -z "${parachain}" ]]; then
    echo "parachain ID not provided. Please add --parachain flag with parachain value [--parachain 4222]"
    exit_clean_hist
  fi

  mkdir -p $OUTPUT_DIR

  specfileName="chain-spec-$parachain.json"

  if test -f "$OUTPUT_DIR/$specfileName"; then
    echo " "
    echo "================================================"
    echo "  Spec exist."
    echo "  Open it and modify it according to your needs."
    echo "  Then come back here and continue."
    echo "================================================"
    echo " "
  else
    echo "Generating the spec file in $OUTPUT_DIR/$specfileName"

    cmd="docker run --rm $image build-spec --chain=$chain --disable-default-bootnode > $OUTPUT_DIR/$specfileName"
    echo "$cmd"
    docker run --rm $image build-spec --chain=$chain --disable-default-bootnode >$OUTPUT_DIR/$specfileName

    echo " "
    echo "================================================"
    echo "  Spec generated."
    echo "  Open it and modify it according to your needs."
    echo "  Then come back here and continue."
    echo "  DO NOT RUN THIS COMAND AGAIN YOU WILL LOSE ALL YOUR WORK"
    echo "================================================"
    echo " "
  fi

}

function create_raw() {
  echo "Generating RAW spec, verification code and genesis state."
  if [[ -z "${parachain}" ]]; then
    echo "parachain ID not provided. Please add --parachain flag with parachain value [--parachain 4222]"
    exit_clean_hist
  fi
  specfileName="chain-spec-$parachain.json"
  rawName="chain-raw-$parachain.json"
  wasmFilename="para-$parachain-wasm.txt"
  stateFilename="para-$parachain-state.txt"

  echo "  build-spec --raw"
  docker run -v "$(pwd)/"$OUTPUT_DIR"":/app --rm $image build-spec --raw --chain=/app/$specfileName --disable-default-bootnode >$OUTPUT_DIR/$rawName

  echo "  export-genesis-wasm"
  docker run -v "$(pwd)/"$OUTPUT_DIR"":/app --rm $image export-genesis-wasm --chain=/app/$rawName >$OUTPUT_DIR/$wasmFilename
  echo "    verificationCode: $OUTPUT_DIR/$wasmFilename"

  echo "  export-genesis-state"
  docker run -v "$(pwd)/"$OUTPUT_DIR"":/app --rm $image export-genesis-state --chain=/app/$rawName >$OUTPUT_DIR/$stateFilename
  echo "    genesisState: $OUTPUT_DIR/$stateFilename"

  exit 0
}
echo $chain
if [[ ! -z "${chain}" ]]; then

  create_spec

  echo "I have updated spec file, please generate raw genesis."
  select yn in "Yes" "No"; do
    case $yn in
    Yes) create_raw ;;
    No) exit ;;
    esac
  done
fi
